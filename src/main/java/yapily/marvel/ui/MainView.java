package yapily.marvel.ui;


import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import yapily.marvel.backend.CharacterRepositoryService;
import yapily.marvel.model.MarvelCharacter;

import java.util.List;

@Route("")
@Push
@JavaScript("frontend://js/marvel.js")
@HtmlImport("frontend://styles/marvelgrid-styles.html")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@BodySize(height = "100vh", width = "100vw")
public class MainView extends VerticalLayout {
    public static final String IMAGE_KEY_ID = "image";
    private VerticalLayout layout;
    private Grid<MarvelCharacter> grid;
    final static int PAGE_LIMIT = 10;
    private Button firstButton;
    private Button lastButton;
    private Button nextButton;
    private Button previousButton;
    private Button searchButton;
    private Integer currentIndex;
    private TextField searchField;
    private LayoutMode currentMode;
    private int[] currentIndexRange;
    private int windowWidth;
    private boolean isPreviousWidthZero;
    private CharacterRepositoryService characterRepositoryService;
    public static final org.apache.logging.log4j.Logger log = LogManager.getLogger();

    public enum LayoutMode {

        MOBILE, DESKTOP
    }

    public MainView(@Autowired CharacterRepositoryService characterRepositoryServiceImpl) throws Exception {
        try {
            this.characterRepositoryService = characterRepositoryServiceImpl;
            if (characterRepositoryService.countCharacters() == 0) {
                characterRepositoryService.loadRepository();
                log.info("Total Number of Items:" + characterRepositoryService.countCharacters());
            }
            currentIndexRange = new int[2];
            isPreviousWidthZero = false;
            layout = new VerticalLayout();
            grid = new Grid<>(MarvelCharacter.class);
            firstPage();
            firstButton = new Button("First", l -> firstPage());
            lastButton = new Button("Last", l -> lastPage());
            nextButton = new Button("Previous", l -> previousPage(getCurrentIndex()));
            previousButton = new Button("Next", l -> nextPage(getCurrentIndex()));
            searchField = new TextField("Search");
            searchButton = new Button("Search", l -> searchResults(searchField.getValue()));
            HorizontalLayout pages = new HorizontalLayout();
            pages.add(firstButton);
            pages.add(previousButton);
            pages.add(nextButton);
            pages.add(lastButton);
            HorizontalLayout search = new HorizontalLayout();
            search.add(searchField);
            search.add(searchButton);
            search.setAlignItems(Alignment.BASELINE);
            layout.add(search);
            layout.add(grid);
            layout.add(pages);
            add(layout);
            getStyle().set("background-color", "#DCDCDC");
            responsiveGrid();
            UI.getCurrent().getPage().addBrowserWindowResizeListener(e -> {
                responsiveGrid();
            });
        } catch (Exception e) {
            log.error("Failed to execute MainView init:", e);
        }


    }

    private void firstPage() {
        int endIndex = PAGE_LIMIT > characterRepositoryService.countCharacters() ? characterRepositoryService.countCharacters() : PAGE_LIMIT;
        setCurrentIndex(endIndex);
        currentIndexRange = new int[]{0, endIndex};
        grid.setDataProvider(new ListDataProvider<>(characterRepositoryService.getCharacters(0, endIndex)));
        if (grid.getColumnByKey("id") != null) {
            grid.removeColumnByKey("id");
        }

        if (grid.getColumnByKey("imageUrl") != null) {
            grid.removeColumnByKey("imageUrl");
        }
        grid.setColumns("name", "description");
        grid.addColumn(new ComponentRenderer<>(marvelCharacter -> {
            Image image = new Image();
            image.setSrc(marvelCharacter.getImageUrl());
            return image;
        })).setHeader("Image").setKey(IMAGE_KEY_ID).setId(IMAGE_KEY_ID);
        grid.addComponentColumn(character -> {
            RouterLink button = new RouterLink("More Details",
                    CharacterDetailsView.class,
                    character.getId());
            button.getStyle().set("min-width", "25px");
            button.getStyle().set("max-width", "35px");
            return button;
        }).setKey("moredetails").setWidth("50px");


    }

    private void lastPage() {
        setCurrentIndex(characterRepositoryService.countCharacters());
        int remainder = characterRepositoryService.countCharacters() % PAGE_LIMIT;
        currentIndexRange = new int[]{characterRepositoryService.countCharacters() - (remainder + 1), characterRepositoryService.countCharacters()};
        ListDataProvider lp = DataProvider.ofCollection(characterRepositoryService.getCharacters(characterRepositoryService.countCharacters() - (remainder + 1), characterRepositoryService.countCharacters()));
        grid.setDataProvider(lp);
        grid.getDataProvider().refreshAll();
    }

    private void nextPage(int begin) {
        if (begin <= characterRepositoryService.countCharacters() - 1) {
            int endIndex = begin + PAGE_LIMIT > characterRepositoryService.countCharacters() ? characterRepositoryService.countCharacters() : begin + PAGE_LIMIT;
            setCurrentIndex(endIndex);
            currentIndexRange = new int[]{begin, endIndex};
            ListDataProvider lp = DataProvider.ofCollection(characterRepositoryService.getCharacters(begin, endIndex));
            grid.setDataProvider(lp);
            grid.getDataProvider().refreshAll();
        }
    }

    private void previousPage(int begin) {
        if (begin > 0) {
            int beginIndex = begin - PAGE_LIMIT >= 0 ? begin - PAGE_LIMIT : 0;
            int endIndex = begin;
            setCurrentIndex(beginIndex);
            currentIndexRange = new int[]{begin, endIndex};
            ListDataProvider lp = DataProvider.ofCollection(characterRepositoryService.getCharacters(beginIndex, endIndex));
            grid.setDataProvider(lp);
            grid.getDataProvider().refreshAll();
        }
    }

    private void searchResults(String searchString) {
        if (StringUtils.isNotBlank(searchString)) {
            List<MarvelCharacter> resultList = characterRepositoryService.searchByNameDescription(searchString);
            int endIndex = PAGE_LIMIT > resultList.size() ? resultList.size() : PAGE_LIMIT;
            setCurrentIndex(endIndex);
            currentIndexRange = new int[]{0, endIndex};
            ListDataProvider lp = DataProvider.ofCollection(resultList);
            grid.setDataProvider(lp);
            grid.getDataProvider().refreshAll();
        }
    }

    public void mobileView() {
        if (grid.getColumnByKey(IMAGE_KEY_ID) != null) {
            grid.getColumnByKey(IMAGE_KEY_ID).setWidth("100%");
        }

        if (grid.getColumnByKey("moredetails") != null) {
            grid.getColumnByKey("moredetails").setWidth("100%");
        }

        grid.getDataProvider().refreshAll();
        log.info("mobilegrid invoked");
    }


    private void desktopView() {

        ListDataProvider lp = DataProvider.ofCollection(characterRepositoryService.getCharacters(currentIndexRange[0], currentIndexRange[1]));
        grid.setDataProvider(lp);

        if (grid.getColumnByKey("name") != null) {
            grid.getColumnByKey("name").setWidth("30%");
        }
        if (grid.getColumnByKey("description") != null) {
            grid.getColumnByKey("description").setWidth("30%");
        }

        if (grid.getColumnByKey(IMAGE_KEY_ID) != null) {
            grid.getColumnByKey(IMAGE_KEY_ID).setWidth("30%");
        }

        if (grid.getColumnByKey("moredetails") != null) {
            grid.getColumnByKey("moredetails").setWidth("30%");
        }

        grid.getDataProvider().refreshAll();

    }

    public Integer getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(Integer currentIndex) {
        this.currentIndex = currentIndex;
    }

    public void responsiveGrid() {
        LayoutMode previousMode = currentMode;
        getElement().executeJavaScript("javascriptFunction($0)", getElement());

        if (getWindowWidth() == 0) {
            log.info("layoutmode for zero;" + LayoutMode.DESKTOP.name());
            currentMode = LayoutMode.DESKTOP;
            isPreviousWidthZero = true;
        } else if (getWindowWidth() > 0 && getWindowWidth() < 320) {
            isPreviousWidthZero = Boolean.FALSE;
            log.info("layoutmode ;" + LayoutMode.MOBILE.name());
            currentMode = LayoutMode.MOBILE;
            if (previousMode != null && !LayoutMode.MOBILE.name().equals(previousMode.name())) {
                mobileView();
            }
        } else {
            isPreviousWidthZero = Boolean.FALSE;
            log.info("layoutmode ;" + LayoutMode.DESKTOP.name());
            currentMode = LayoutMode.DESKTOP;
            if (previousMode != null && !LayoutMode.DESKTOP.name().equals(previousMode.name())) {
                desktopView();
            }
        }
    }

    @ClientCallable
    private void javaFunction(int width) {
        setWindowWidth(width);
        log.info("javascript width" + width);

    }


    public int getWindowWidth() {
        return windowWidth;

    }


    public void setWindowWidth(int windowWidth) {
        if (null == Integer.valueOf(windowWidth)) {
            windowWidth = 0;
        }
        this.windowWidth = windowWidth;
        if (isPreviousWidthZero) {
            responsiveGrid();
        }


    }
}
