package yapily.marvel.ui;

import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import yapily.marvel.backend.CharacterRepositoryService;
import yapily.marvel.model.MarvelCharacterDetails;

import java.util.LinkedHashMap;
import java.util.List;

@Route("details")
@HtmlImport("frontend://styles/marveldetails-styles.html")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@BodySize(height = "100vh", width = "100vw")
public class CharacterDetailsView extends VerticalLayout implements HasUrlParameter<Long> {
    public static final String PIXELS_300 = "300px";
    public static final String PIXELS_320 = "320px";
    public static final String MAX_HEIGHT_STYLE_STRING = "max-height";
    public static final String PIXELS_400 = "400px";
    public static final String PIXELS_100 = "100px";
    private Grid<MarvelCharacterDetails> grid;

    private CharacterRepositoryService characterRepositoryService;
    private VerticalLayout layout;
    public static final org.apache.logging.log4j.Logger log = LogManager.getLogger();

    public CharacterDetailsView(@Autowired CharacterRepositoryService characterRepositoryServiceImpl) {
        try {
            layout = new VerticalLayout();
            grid = new Grid<>(MarvelCharacterDetails.class);
            this.characterRepositoryService = characterRepositoryServiceImpl;
            NativeButton button = new NativeButton("Back");
            button.addClickListener(e -> {
                button.getUI().ifPresent(ui -> ui.navigate(MainView.class));
            });
            layout.add(grid);
            add(layout, button);
            getStyle().set("background-color", "#778899");
        } catch (Exception e) {
            log.error("Failed to execute CharacterDetailsView init:", e);
        }
    }

    @Override
    public void setParameter(BeforeEvent event, Long parameter) {
        try {
            characterRepositoryService.getCharacterDetail(parameter)
                    .ifPresent(c -> generateDetailsGrid(c));
        } catch (Exception e) {
            log.error("Failed to execute CharacterDetailsView setParameter(BeforeEvent event, Long parameter):", e);
        }
    }

    public void generateDetailsGrid(MarvelCharacterDetails details) {
        grid.setItems(details);
        grid.removeColumnByKey("id");
        grid.removeColumnByKey("name");
        grid.removeColumnByKey("stories");
        grid.removeColumnByKey("events");
        grid.removeColumnByKey("series");
        grid.addComponentColumn(marvelCharacterDetails -> {
            TextArea storiesArea = new TextArea("");
            storiesArea.setValue(joinEntrySets(marvelCharacterDetails.getStories()));
            storiesArea.setReadOnly(Boolean.TRUE);
            storiesArea.setMinWidth(PIXELS_320);
            storiesArea.getStyle().set(MAX_HEIGHT_STYLE_STRING, PIXELS_400);
            storiesArea.setMinHeight(PIXELS_300);
            storiesArea.getStyle().set(MAX_HEIGHT_STYLE_STRING, PIXELS_100);
            return storiesArea;
        }).setHeader("Stories").setWidth(PIXELS_400);


        grid.addComponentColumn(marvelCharacterDetails -> {
            TextArea eventsArea = new TextArea("");
            eventsArea.setValue(joinEntrySets(marvelCharacterDetails.getEvents()));
            eventsArea.setReadOnly(Boolean.TRUE);
            eventsArea.setMinWidth(PIXELS_320);
            eventsArea.getStyle().set(MAX_HEIGHT_STYLE_STRING, PIXELS_400);
            eventsArea.setMinHeight(PIXELS_300);
            eventsArea.getStyle().set(MAX_HEIGHT_STYLE_STRING, PIXELS_100);
            return eventsArea;
        }).setHeader("Events").setWidth(PIXELS_400);


        grid.addComponentColumn(marvelCharacterDetails -> {
            TextArea seriesArea = new TextArea("");
            seriesArea.setValue(joinEntrySets(marvelCharacterDetails.getSeries()));
            seriesArea.setReadOnly(Boolean.TRUE);
            seriesArea.setMinWidth(PIXELS_320);
            seriesArea.setMinHeight(PIXELS_300);
            seriesArea.getStyle().set(MAX_HEIGHT_STYLE_STRING, PIXELS_400);
            return seriesArea;
        }).setHeader("Series").setWidth(PIXELS_400);

    }

    private String joinEntrySets(List<LinkedHashMap<String, Object>> detailList) {
        StringBuilder detailBuilder = new StringBuilder("");
        detailList.stream().forEach(
                detail -> {
                    detail.entrySet().stream().forEach(
                            entry -> {
                                detailBuilder.append(entry.getKey());
                                detailBuilder.append(":");
                                detailBuilder.append(String.valueOf(entry.getValue()));
                                detailBuilder.append(String.format("%n"));
                            });
                });
        return detailBuilder.toString();
    }


}
