package yapily.marvel.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MarvelCharacterDetails {
    private Long id;
    private String name;
    private List<LinkedHashMap<String, Object>> stories;
    private List<LinkedHashMap<String, Object>> events;
    private List<LinkedHashMap<String, Object>> series;

    public List<LinkedHashMap<String, Object>> getStories() {
        if (stories == null) {
            stories = new ArrayList();
        }
        return stories;
    }

    public void setStories(List<LinkedHashMap<String, Object>> stories) {
        this.stories = stories;
    }

    public List<LinkedHashMap<String, Object>> getEvents() {
        if (events == null) {
            events = new ArrayList();
        }
        return events;
    }

    public void setEvents(List<LinkedHashMap<String, Object>> events) {
        this.events = events;
    }

    public List<LinkedHashMap<String, Object>> getSeries() {
        if (series == null) {
            series = new ArrayList();
        }
        return series;
    }

    public void setSeries(List<LinkedHashMap<String, Object>> series) {
        this.series = series;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
