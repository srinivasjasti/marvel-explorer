package yapily.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class MarvelCharacter {

    Long id;

    String name;

    String description;

    String imageUrl;

}
