package yapily.marvel.backend;

import yapily.marvel.model.MarvelCharacter;
import yapily.marvel.model.MarvelCharacterDetails;

import java.util.List;
import java.util.Optional;

public interface CharacterRepositoryService {

    List<MarvelCharacter> getCharacters(int offset, int limit);

    List<MarvelCharacterDetails> getCharacterDetails(int offset, int limit);

    void loadRepository() throws Exception;

    Optional<MarvelCharacter> getCharacter(Long id);

    List<MarvelCharacter> searchByNameDescription(String searchString);

    Optional<MarvelCharacterDetails> getCharacterDetail(Long id);

    Integer countCharacters();

}
