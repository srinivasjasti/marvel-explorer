package yapily.marvel.backend;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import yapily.marvel.model.MarvelCharacter;
import yapily.marvel.model.MarvelCharacterDetails;

import javax.xml.bind.DatatypeConverter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class CharacterRepositoryServiceImpl implements CharacterRepositoryService {
    public static final String MARVEL_API_BASE_URI = "https://gateway.marvel.com:443/v1/public/characters";
    public static final int API_CALL_LIMIT_100 = 100;
    public static final int OFFSET_ZERO = 0;
    public static final int FIRST_CALL = 1;
    public static final int ROUND_UP_SCALE_ZERO = 0;
    public static final String ID_STRING = "id";
    public static final String NAME_STRING = "name";
    public static final String DESCRIPTION_STRING = "description";
    public static final String THUMBNAIL_STRING = "thumbnail";
    public static final String PATH_STRING = "path";
    public static final String URI_SEPARATOR = "/";
    public static final String IMAGE_TYPE_PORTRAIT_XLARGE = "portrait_xlarge.";
    public static final String IMAGE_EXTENSION = "extension";
    public static final String ITEMS_STRING = "items";
    public static final String STORIES_STRING = "stories";
    public static final String EVENTS_STRING = "events";
    public static final String SERIES_STRING = "series";
    public static final String DIGEST_INSTANCE_MD_5 = "MD5";
    public static final String URI_QUERY_STRING = "?";
    public static final String URI_PARAM_LIMIT = "limit=";
    public static final String URI_PARAM_OFFSET = "&offset=";
    public static final String URI_PARAM_TIMESTAMP = "&ts=";
    public static final String URI_PARAM_API_KEY = "&apikey=";
    public static final String URI_PARAM_HASH = "&hash=";
    private static final List<MarvelCharacter> characterRepository = new ArrayList<>();
    private static final List<MarvelCharacterDetails> characterDetailsRepository = new ArrayList<>();
    public static final org.apache.logging.log4j.Logger log = LogManager.getLogger();

    @Autowired
    private WebClient webClient;

    @Value("${marvel.api.public.key}")
    private String publicKey;

    @Value("${marvel.api.private.key}")
    private String privateKey;

    @Override
    public List<MarvelCharacter> getCharacters(int offset, int limit) {
        List<MarvelCharacter> result = new ArrayList();
        try {
            if (offset < characterRepository.size()) {
                int toIndex = offset + limit;
                if (toIndex > characterRepository.size()) {
                    toIndex = characterRepository.size();
                }
                result = characterRepository.subList(offset, toIndex);
            } else {
                result = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error("Failed to execute getCharacters(int offset, int limit):", e);
        }
        return result;
    }

    @Override
    public List<MarvelCharacterDetails> getCharacterDetails(int offset, int limit) {
        List<MarvelCharacterDetails> result = new ArrayList();
        try {
            if (offset < characterDetailsRepository.size()) {
                int toIndex = offset + limit;
                if (toIndex > characterDetailsRepository.size()) {
                    toIndex = characterDetailsRepository.size();
                }
                result = characterDetailsRepository.subList(offset, toIndex);
            } else {
                result = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error("Failed to execute getCharacterDetails(int offset, int limit):", e);
        }

        return result;
    }

    @Override
    public void loadRepository() throws Exception {
        int limit = API_CALL_LIMIT_100;
        int offset = OFFSET_ZERO;
        final long currentTime = Instant.now().getEpochSecond();
        try {
            HashMap<String, Object> data = executeWebClient(limit, offset, currentTime);
            int total = Integer.valueOf(String.valueOf(data.get("total")));
            uploadRepositories(data, characterRepository, characterDetailsRepository);
            int numberOfCallsValue = calculateCalls(limit, total);
            log.info("NumberOfcalls:" + numberOfCallsValue);
            IntStream.range(FIRST_CALL, numberOfCallsValue).parallel().forEach(call -> {
                log.info("ApiCallNumber:" + call);
                int offsetValue = call * limit;
                log.info("OffSetValue:" + offsetValue);
                try {
                    HashMap<String, Object> subdata = executeWebClient(limit, offsetValue, currentTime);
                    uploadRepositories(subdata, characterRepository, characterDetailsRepository);
                } catch (Exception e) {
                    log.error("Failed to execute MarvelApiCall:", e);
                }

            });
        } catch (Exception e) {
            log.error("Failed to execute MarvelApiCall:", e);
        }

    }


    @Override
    public Optional<MarvelCharacter> getCharacter(Long id) {
        Optional<MarvelCharacter> marvelCharacter = Optional.ofNullable(null);
        try {
            marvelCharacter = characterRepository.stream()
                    .filter(c -> c.getId().equals(id))
                    .findAny();
        } catch (Exception e) {
            log.error("Failed to executegetCharacter(Long id):", e);
        }
        return marvelCharacter;
    }

    @Override
    public List<MarvelCharacter> searchByNameDescription(String searchString) {
        List<MarvelCharacter> result = new ArrayList();

        try {
            result = characterRepository.stream()
                    .filter(e -> {
                        return StringUtils.deleteWhitespace(e.getName()).equalsIgnoreCase(StringUtils.deleteWhitespace(searchString)) || e.getDescription().equalsIgnoreCase(searchString);

                    }).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Failed to execute searchByNameDescription(String searchString):", e);
        }
        return result;
    }

    @Override
    public Optional<MarvelCharacterDetails> getCharacterDetail(Long id) {
        Optional<MarvelCharacterDetails> marvelCharacterDetails = Optional.ofNullable(null);
        try {
            marvelCharacterDetails = characterDetailsRepository.stream()
                    .filter(c -> c.getId().equals(id))
                    .findAny();
        } catch (Exception e) {
            log.error("Failed to execute getCharacterDetail(Long id):", e);
        }

        return marvelCharacterDetails;
    }

    @Override
    public Integer countCharacters() {
        Integer count = 0;
        try {
            count = characterRepository.size();
        } catch (Exception e) {
            log.error("Failed to execute countCharacters():", e);
        }
        return count;
    }


    private int calculateCalls(int limit, int total) {
        Double totalValue = Double.valueOf(total);
        Double limitValue = Double.valueOf(limit);
        BigDecimal numberOfCalls = BigDecimal.valueOf(totalValue / limitValue);
        numberOfCalls = numberOfCalls.setScale(ROUND_UP_SCALE_ZERO, RoundingMode.UP);
        return numberOfCalls.intValue();
    }

    private HashMap<String, Object> executeWebClient(int limit, int offset, long currentTime) throws Exception {
        String theUrl = buildMarvelAPIUrl(limit, offset, currentTime);
        Mono<Map> jsonMapMono = webClient.get()
                .uri(theUrl, "1")
                .exchange()
                .flatMap(response -> response.bodyToMono(Map.class));
        Map response = jsonMapMono.block();
        return (HashMap<String, Object>) response.get("data");

    }

    private String buildMarvelAPIUrl(int limit, int offset, long currentTime) throws Exception {
        StringBuilder uriBuilder = new StringBuilder();
        uriBuilder.append(MARVEL_API_BASE_URI);
        uriBuilder.append(URI_QUERY_STRING);
        uriBuilder.append(URI_PARAM_LIMIT);
        uriBuilder.append(limit);
        uriBuilder.append(URI_PARAM_OFFSET);
        uriBuilder.append(offset);
        uriBuilder.append(URI_PARAM_TIMESTAMP);
        uriBuilder.append(currentTime);
        uriBuilder.append(URI_PARAM_API_KEY);
        uriBuilder.append(publicKey);
        uriBuilder.append(URI_PARAM_HASH);
        uriBuilder.append(getHashKey(currentTime));
        return uriBuilder.toString();
    }

    private String getHashKey(long currentTime) throws Exception {
        StringBuilder toBeDigested = new StringBuilder();
        toBeDigested.append(currentTime);
        toBeDigested.append(privateKey);
        toBeDigested.append(publicKey);
        MessageDigest messageDigest = MessageDigest.getInstance(DIGEST_INSTANCE_MD_5);
        messageDigest.update(toBeDigested.toString().getBytes(StandardCharsets.UTF_8));
        byte[] digest = messageDigest.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }


    public void uploadRepositories(Map<String, Object> data, List<MarvelCharacter> characterList, List<MarvelCharacterDetails> characterDetailsList) {
        ArrayList<LinkedHashMap<String, Object>> results = (ArrayList<LinkedHashMap<String, Object>>) data.get("results");
        results.stream().forEach(result -> {
            MarvelCharacterDetails details = new MarvelCharacterDetails();
            Long id = Long.valueOf(String.valueOf(result.get(ID_STRING)));
            String name = String.valueOf(result.get(NAME_STRING));
            String description = String.valueOf(result.get(DESCRIPTION_STRING));
            Map<String, String> imageUrlMap = (Map<String, String>) result.get(THUMBNAIL_STRING);
            StringBuilder imageUrl = new StringBuilder();
            imageUrl.append(imageUrlMap.get(PATH_STRING));
            imageUrl.append(URI_SEPARATOR);
            imageUrl.append(IMAGE_TYPE_PORTRAIT_XLARGE);
            imageUrl.append(imageUrlMap.get(IMAGE_EXTENSION));

            MarvelCharacter character = new MarvelCharacter(id, name, description, imageUrl.toString());
            characterList.add(character);
            details.setName(name);
            details.setId(id);
            LinkedHashMap<String, Object> storiesMap = (LinkedHashMap<String, Object>) result.get(STORIES_STRING);
            ArrayList<LinkedHashMap<String, Object>> stories = (ArrayList<LinkedHashMap<String, Object>>) storiesMap.get(ITEMS_STRING);
            details.setStories(stories);
            LinkedHashMap<String, Object> eventsMap = (LinkedHashMap<String, Object>) result.get(EVENTS_STRING);
            ArrayList<LinkedHashMap<String, Object>> events = (ArrayList<LinkedHashMap<String, Object>>) eventsMap.get(ITEMS_STRING);
            details.setEvents(events);
            LinkedHashMap<String, Object> seriesMap = (LinkedHashMap<String, Object>) result.get(SERIES_STRING);
            ArrayList<LinkedHashMap<String, Object>> series = (ArrayList<LinkedHashMap<String, Object>>) seriesMap.get(ITEMS_STRING);
            details.setSeries(series);
            characterDetailsList.add(details);

        });

    }

    public static List<MarvelCharacter> getCharacterRepository() {
        return characterRepository;
    }

    public static List<MarvelCharacterDetails> getCharacterDetailsRepository() {
        return characterDetailsRepository;
    }

}
