package yapily.marvel.ui;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import yapily.marvel.backend.CharacterRepositoryService;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class MainViewTest {

    @Mock
    CharacterRepositoryService characterRepositoryServiceImpl;
    @Mock
    MainView mainView;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
        mainView = null;
    }

    @Test
    public void getCurrentIndex() {
        assertTrue(mainView.getCurrentIndex() == 0);

    }

    @Test
    public void getWindowWidth() {
        assertTrue(mainView.getWindowWidth() == 0);
    }


}