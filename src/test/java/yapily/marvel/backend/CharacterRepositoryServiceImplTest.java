package yapily.marvel.backend;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.reactive.function.client.WebClient;
import yapily.marvel.Application;
import yapily.marvel.model.MarvelCharacter;
import yapily.marvel.model.MarvelCharacterDetails;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Application.class})
@WebAppConfiguration
public class CharacterRepositoryServiceImplTest {

    private CharacterRepositoryServiceImpl characterRepositoryServiceImpl;

    @Autowired
    private WebClient webClient;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        characterRepositoryServiceImpl = new CharacterRepositoryServiceImpl();
    }

    @After
    public void tearDown() throws Exception {
        webClient = null;
        characterRepositoryServiceImpl = null;
    }


    @Test
    public void testUploadRepositories() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        CharacterRepositoryServiceImpl.getCharacterRepository().clear();
        CharacterRepositoryServiceImpl.getCharacterDetailsRepository().clear();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        assertTrue(data.size() == 5);
        assertTrue(CharacterRepositoryServiceImpl.getCharacterRepository().size() == 20);
        assertTrue(CharacterRepositoryServiceImpl.getCharacterDetailsRepository().size() == 20);
    }


    @Test
    public void testCountCharacters() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        CharacterRepositoryServiceImpl.getCharacterRepository().clear();
        CharacterRepositoryServiceImpl.getCharacterDetailsRepository().clear();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        assertTrue(characterRepositoryServiceImpl.countCharacters() == 20);
    }

    @Test
    public void testGetCharacter() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        CharacterRepositoryServiceImpl.getCharacterRepository().clear();
        CharacterRepositoryServiceImpl.getCharacterDetailsRepository().clear();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        Optional<MarvelCharacter> character = characterRepositoryServiceImpl.getCharacter(Long.valueOf("1011334"));
        assertTrue(character.isPresent());

    }

    @Test
    public void testGetCharacterDetail() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        CharacterRepositoryServiceImpl.getCharacterRepository().clear();
        CharacterRepositoryServiceImpl.getCharacterDetailsRepository().clear();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        Optional<MarvelCharacterDetails> characterDetail = characterRepositoryServiceImpl.getCharacterDetail(Long.valueOf("1011334"));
        assertTrue(characterDetail.isPresent());
    }

    @Test
    public void testGetCharacterDetails() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        CharacterRepositoryServiceImpl.getCharacterRepository().clear();
        CharacterRepositoryServiceImpl.getCharacterDetailsRepository().clear();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        List<MarvelCharacterDetails> characterDetailList = characterRepositoryServiceImpl.getCharacterDetails(0, 5);
        assertTrue(characterDetailList.size() == 5);
    }

    @Test
    public void testGetCharacters() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        List<MarvelCharacterDetails> details = new ArrayList();
        List<MarvelCharacter> characters = new ArrayList();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        List<MarvelCharacter> charactersList = characterRepositoryServiceImpl.getCharacters(0, 5);
        assertTrue(charactersList.size() == 5);
    }

    @Test
    public void testSearchByNameDescription() throws Exception {
        HashMap<String, Object> data = getApiMockData();
        CharacterRepositoryServiceImpl.getCharacterRepository().clear();
        CharacterRepositoryServiceImpl.getCharacterDetailsRepository().clear();
        characterRepositoryServiceImpl.uploadRepositories(data, CharacterRepositoryServiceImpl.getCharacterRepository(), CharacterRepositoryServiceImpl.getCharacterDetailsRepository());
        List<MarvelCharacter> charactersList = characterRepositoryServiceImpl.searchByNameDescription("3-dman");
        assertTrue(charactersList.size() == 1);
    }

    private HashMap<String, Object> getApiMockData() throws IOException {
        FileInputStream fis = new FileInputStream("src/test/resources/marvel-api-response.txt");
        String jsonAsString = IOUtils.toString(fis, StandardCharsets.UTF_8);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
        };
        HashMap<String, Object> readValues = mapper.readValue(jsonAsString, typeRef);
        return (HashMap<String, Object>) readValues.get("data");
    }

}
