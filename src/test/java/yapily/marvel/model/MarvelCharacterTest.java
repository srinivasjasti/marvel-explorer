package yapily.marvel.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MarvelCharacterTest {
    private MarvelCharacter marvelCharacter;

    @Before
    public void setUp() throws Exception {
        marvelCharacter = MarvelCharacter
                .builder()
                .id(1L)
                .name("name")
                .description("description")
                .imageUrl("imageurl")
                .build();
    }

    @After
    public void tearDown() throws Exception {
        marvelCharacter = null;
    }

    @Test
    public void getId() {
        assertTrue(marvelCharacter.getId() == 1L);
    }

    @Test
    public void getName() {
        assertTrue(marvelCharacter.getName() == "name");
    }

    @Test
    public void getDescription() {
        assertTrue(marvelCharacter.getDescription() == "description");
    }

    @Test
    public void getImageUrl() {
        assertTrue(marvelCharacter.getImageUrl() == "imageurl");
    }

    @Test
    public void setId() {
        marvelCharacter.setId(2L);
        assertTrue(marvelCharacter.getId() == 2L);
    }

    @Test
    public void setName() {
        marvelCharacter.setName("newname");
        assertTrue(marvelCharacter.getName() == "newname");
    }

    @Test
    public void setDescription() {
        marvelCharacter.setDescription("newdescription");
        assertTrue(marvelCharacter.getDescription() == "newdescription");
    }

    @Test
    public void setImageUrl() {
        marvelCharacter.setImageUrl("newimageurl");
        assertTrue(marvelCharacter.getImageUrl() == "newimageurl");
    }
}