package yapily.marvel.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MarvelCharacterDetailsTest {

    private MarvelCharacterDetails marvelCharacterDetails;
    private ArrayList<LinkedHashMap<String, Object>> values;

    @Before
    public void setUp() throws Exception {
        marvelCharacterDetails = new MarvelCharacterDetails();
        values = new ArrayList();
        LinkedHashMap<String, Object> detail = new LinkedHashMap();
        detail.put("3-D Man", "requesturi");
        values.add(detail);
    }

    @After
    public void tearDown() throws Exception {
        marvelCharacterDetails = null;
    }

    @Test
    public void getStories() {
        assertTrue(CollectionUtils.isEmpty(marvelCharacterDetails.getStories()));
        marvelCharacterDetails.setStories(values);
        assertTrue(marvelCharacterDetails.getStories().size() == 1);
    }

    @Test
    public void setStories() {
        marvelCharacterDetails.setStories(values);
        assertTrue(marvelCharacterDetails.getStories().size() == 1);
    }

    @Test
    public void getEvents() {
        assertTrue(CollectionUtils.isEmpty(marvelCharacterDetails.getEvents()));
        marvelCharacterDetails.setEvents(values);
        assertTrue(marvelCharacterDetails.getEvents().size() == 1);
    }

    @Test
    public void setEvents() {
        marvelCharacterDetails.setEvents(values);
        assertTrue(marvelCharacterDetails.getEvents().size() == 1);

    }

    @Test
    public void getSeries() {
        assertTrue(CollectionUtils.isEmpty(marvelCharacterDetails.getSeries()));
        marvelCharacterDetails.setSeries(values);
        assertTrue(marvelCharacterDetails.getSeries().size() == 1);
    }

    @Test
    public void setSeries() {
        marvelCharacterDetails.setSeries(values);
        assertTrue(marvelCharacterDetails.getSeries().size() == 1);
    }

    @Test
    public void getName() {
        assertNull(marvelCharacterDetails.getName());
        marvelCharacterDetails.setName("name");
        assertTrue(marvelCharacterDetails.getName() == "name");
    }

    @Test
    public void setName() {
        marvelCharacterDetails.setName("newname");
        assertTrue(marvelCharacterDetails.getName() == "newname");
    }

    @Test
    public void getId() {
        assertNull(marvelCharacterDetails.getId());
        marvelCharacterDetails.setId(2L);
        assertTrue(marvelCharacterDetails.getId() == 2L);
    }

    @Test
    public void setId() {
        marvelCharacterDetails.setId(2L);
        assertTrue(marvelCharacterDetails.getId() == 2L);

    }
}